use std::cell::Cell;
use std::sync::mpsc::SyncSender;

use chunk::{self, Chunk, Label, Pixel, Moments};

extern crate r3;
use self::r3::ui::{BB, Px};
use self::r3::ui::draw::DrawCx;

use image::{DynamicImage, GenericImage};

macro_rules! visualize {
    ($($name:expr => $value:expr),*) => {{
        #[allow(unused_imports)]
        use ::visualize::views::*;
        ::visualize::send(vec![$((String::from($name), Box::new($value))),*])
    }}
}

pub trait Visualize {
    /// Draw the object on screen at the specified coordinates
    /// and return the bottom-right corner of the area used.
    fn visualize(&self, cx: &mut DrawCx, pos: [Px; 2]) -> [Px; 2];
}

impl Visualize for Chunk<bool> {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        const SZ: Px = 8.0;

        for row in 0..chunk::SIZE {
            for column in 0..chunk::SIZE {
                let color = self[row][column].to_rgba();
                cx.fill(BB::rect(SZ * column as Px + x, SZ * row as Px + y, SZ, SZ), color);
            }
        }

        [x + SZ * (chunk::SIZE as Px), y + SZ * (chunk::SIZE as Px)]
    }
}

impl<T: Pixel<Row=[T; chunk::SIZE]>> Visualize for [[T; chunk::SIZE]; chunk::SIZE] {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        const SZ: Px = 8.0;

        for row in 0..chunk::SIZE {
            for column in 0..chunk::SIZE {
                let color = self[row][column].to_rgba();
                cx.fill(BB::rect(SZ * column as Px + x, SZ * row as Px + y, SZ, SZ), color);
            }
        }

        [x + SZ * (chunk::SIZE as Px), y + SZ * (chunk::SIZE as Px)]
    }
}

impl Visualize for [Moments; 256] {
    fn visualize(&self, cx: &mut DrawCx, [mut x, sy]: [Px; 2]) -> [Px; 2] {
        let mut y = sy;
        for i in 1..256 {
            if self[i].count() == 0 {
                break;
            }
            let color = Label(i as u8).to_rgba();
            let o = views::Orientation(self[i].orientation().to_angle(), color);
            let [nx, ny] = o.visualize(cx, [x, sy]);
            x = nx + 8.0;
            y = y.max(ny);
        }
        [x, y]
    }
}

pub mod views {
    pub use super::r3::ui::BB;
    pub use image::{ImageLuma8, ImageRgb8, ImageRgba8};

    use super::r3::ui::color::Color;

    pub struct Progress2D {
        pub width: usize,
        pub current: usize,
        pub total: usize
    }

    pub struct Orientation(pub f64, pub Color);
    pub struct Line(pub [f64; 2], pub [f64; 2], pub Color);
    pub struct BoundingBox(pub BB<f64>, pub f64, pub Color);

    pub struct At<T>(pub [f64; 2], pub T);
    pub struct Stacked<A, B>(pub A, pub B);
}

impl Visualize for views::Progress2D {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        const SZ: Px = 4.0;

        // Draw the completed rows.
        let row = self.current / self.width;
        cx.fill(BB::rect(x, y, self.width as Px * SZ, row as Px * SZ),
                [1.0, 1.0, 1.0, 1.0]);

        // Draw the partially completed row.
        let column = self.current % self.width;
        cx.fill(BB::rect(x, y + row as Px * SZ, column as Px * SZ, SZ),
                [1.0, 1.0, 1.0, 1.0]);

        // Draw the current position.
        let column = self.current % self.width;
        cx.fill(BB::rect(x + column as Px * SZ, y + row as Px * SZ, SZ, SZ),
                [0.0, 1.0, 0.0, 1.0]);

        [x + self.width as Px * SZ, y + (self.total / self.width) as Px * SZ]
    }
}

impl Visualize for views::Orientation {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        const RADIUS: Px = 32.0;

        // Coordinates for the center of the circle.
        let [x, y] = [x + RADIUS, y + RADIUS];

        // Grab the angle and compute the coordinates on the circle.
        let a = self.0 as Px;
        let [dx, dy] = [RADIUS * a.cos(), RADIUS * a.sin()];

        // Draw a line between opposite points on the circle.
        cx.line([x - dx, y - dy], [x + dx, y + dy], 4.0, self.1);

        [x + RADIUS, y + RADIUS]
    }
}

impl Visualize for views::Line {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        let [x1, y1] = self.0;
        let [x1, y1] = [x + x1 as Px, y + y1 as Px];
        let [x2, y2] = self.1;
        let [x2, y2] = [x + x2 as Px, y + y2 as Px];
        cx.line([x1, y1], [x2, y2], 2.0, self.2);
        [x1.max(x2), y1.max(y2)]
    }
}

impl Visualize for views::BoundingBox {
    fn visualize(&self, cx: &mut DrawCx, [sx, sy]: [Px; 2]) -> [Px; 2] {
        let bb = self.0.map(|x| x as Px);
        let mut corners = [bb.top_left(), bb.top_right(), bb.bottom_right(), bb.bottom_left()];

        let angle = self.1 as Px;
        let (sin, cos) = (angle.sin(), angle.cos());
        for corner in &mut corners {
            let [x, y] = *corner;
            *corner = [
                sx + x * cos + y * -sin,
                sy + x * sin + y * cos
            ];
        }

        let (mut mx, mut my) = (sx, sy);
        for i in 0..corners.len() {
            cx.line(corners[i], corners[(i + 1) % corners.len()], 2.0, self.2);

            let [x, y] = corners[i];
            mx = x.max(mx);
            my = y.max(my);
        }

        [mx, my]
    }
}

impl Visualize for ::barcode_1d::decode::Line {
    fn visualize(&self, cx: &mut DrawCx, [mut x, y]: [Px; 2]) -> [Px; 2] {
        const HEIGHT: Px = 100.0;

        for i in 0..self.len() {
            let color = if self.value(i) {
                [0.0, 0.0, 0.0, 1.0]
            } else {
                [1.0, 1.0, 1.0, 1.0]
            };
            let w = self.count(i) as Px;
            cx.fill(BB::rect(x, y, w, HEIGHT), color);
            x += w;
        }

        [x, y + HEIGHT]
    }
}

impl<T: Visualize> Visualize for views::At<T> {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        let [dx, dy] = self.0;
        self.1.visualize(cx, [x + dx as Px, y + dy as Px])
    }
}

impl<A: Visualize, B: Visualize> Visualize for views::Stacked<A, B> {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        let [x1, y1] = self.0.visualize(cx, [x, y]);
        let [x2, y2] = self.1.visualize(cx, [x, y]);
        [x1.max(x2), y1.max(y2)]
    }
}

impl<A: Visualize, B: Visualize> Visualize for views::Stacked<A, Vec<B>> {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        let [mut mx, mut my] = self.0.visualize(cx, [x, y]);
        for o in &self.1 {
            let [x, y] = o.visualize(cx, [x, y]);
            mx = x.max(mx);
            my = y.max(my);
        }
        [mx, my]
    }
}

impl Visualize for DynamicImage {
    fn visualize(&self, cx: &mut DrawCx, [x, y]: [Px; 2]) -> [Px; 2] {
        let (w, h) = self.dimensions();
        let bb = BB::rect(x, y, w as Px, h as Px);
        cx.textured(bb, self.to_rgba());
        bb.bottom_right()
    }
}

pub type Row = Vec<(String, Box<Visualize+Send>)>;

pub enum Action {
    Push(Row),
    // The number is the distance from the corresponding
    // push action for this row.
    Pop(usize)
}

scoped_thread_local!(static SENDER: SyncSender<Action>);
thread_local!(static COUNT: Cell<usize> = Cell::new(0));

/// Set up the channel for communicating with the visualizer.
/// The visualizer interface will only work inside the given
/// closure, and the SyncSender<Action> will be used to send
/// all visualizer actions from this thread.
pub fn enter<F: FnOnce() -> R, R>(tx: SyncSender<Action>, f: F) -> R {
    SENDER.set(&tx, f)
}

/// Send a row of named objects to the visualizer.
/// The returned value will trigger an Action::Pop as soon
/// as it is dropped.
pub fn send(row: Row) -> PopGuard {
    SENDER.with(|tx| tx.send(Action::Push(row)).unwrap());
    PopGuard {
        count: COUNT.with(|count| {
            let current = count.get();
            count.set(current + 1);
            current
        })
    }
}

pub struct PopGuard {
    count: usize
}

impl Drop for PopGuard {
    fn drop(&mut self) {
        SENDER.with(|tx| {
            let distance = COUNT.with(|count| {
                let current = count.get();
                count.set(current + 1);
                current - self.count
            });
            tx.send(Action::Pop(distance)).unwrap();
        });
    }
}
