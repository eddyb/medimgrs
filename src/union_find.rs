pub trait UnionFind {
    type L: Copy + Ord;
    fn get(&mut self, Self::L) -> &mut Self::L;
    fn union(&mut self, a: Self::L, b: Self::L) {
        let x = self.find(a);
        let y = self.find(b);
        if x < y {
            *self.get(y) = x;
            *self.get(b) = x;
        } else {
            *self.get(x) = y;
            *self.get(a) = y;
        }
    }
    fn find(&mut self, l: Self::L) -> Self::L {
        let mut x = *self.get(l);
        if x != l {
            x = self.find(x);
            *self.get(l) = x;
        }
        x
    }
}

impl UnionFind for Vec<usize> {
    type L = usize;
    fn get(&mut self, l: usize) -> &mut usize { &mut self[l] }
}

#[test]
fn test() {
    impl UnionFind for [usize; 6] {
        type L = usize;
        fn get(&mut self, l: usize) -> &mut usize { &mut self[l] }
    }
    let mut map = [0, 1, 2, 3, 4, 5];
    map.union(0, 1);
    map.union(1, 2);
    map.union(0, 2);
    map.union(3, 4);
    map.union(2, 3);
    map.union(3, 5);
    for i in 0..5 {
        assert_eq!(map.find(i), map.find(i+1));
    }
}
