extern crate image;
extern crate time;

extern crate medimgrs;

use std::collections::VecDeque;
use std::fs;

fn main() {
    let dir = std::env::args().nth(1).expect("Missing directory path");
    let mut entries: VecDeque<_> = fs::read_dir(dir).unwrap().collect();
    while let Some(entry) = entries.pop_front() {
        let entry = entry.unwrap();
        let path = entry.path();
        if entry.file_type().unwrap().is_dir() {
            entries.extend(fs::read_dir(path).unwrap());
            continue;
        }

        if path.extension().map_or(true, |ext| ext != "png") {
            continue;
        }

        let image = image::open(&path).unwrap().to_luma();

        let start = time::precise_time_ns();
        let results = medimgrs::barcode_2d::qr::find(&image);
        let end = time::precise_time_ns();
        let duration = (end - start) as f64 / 1e6;
        println!("{:03.4}ms {:?}: {:?}", duration, path, results);
    }
}
