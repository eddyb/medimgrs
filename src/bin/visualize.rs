#![feature(slice_patterns)]

extern crate glium;
use glium::DisplayBuild;

extern crate time;

#[macro_use]
extern crate r3;
pub use r3::{cfg, ui};
use r3::glyph::GlyphCache;

use ui::Px;
use ui::color::Scheme;
use ui::draw::DrawCx;
use ui::event::Dispatch;
use ui::layout::Layout;
use ui::text::FontFaces;

use std::cell::{Cell, RefCell};
use std::sync::mpsc;
use std::thread;

use medimgrs::visualize::{self, Action, Visualize};
extern crate medimgrs;
extern crate image;

fn main() {
    let display = glium::glutin::WindowBuilder::new()
        .with_dimensions(800, 600)
        .with_title(String::from("visualizer"))
        .build_glium()
        .unwrap();

    let renderer = &mut ui::render::Renderer::new(&display, FontFaces {
        regular: GlyphCache::from_data(include_bytes!("../../assets/NotoSans/NotoSans-Regular.ttf"), display.clone()).unwrap(),
        mono: GlyphCache::from_data(include_bytes!("../../assets/Hasklig/Hasklig-Regular.otf"), display.clone()).unwrap(),
        mono_bold: GlyphCache::from_data(include_bytes!("../../assets/Hasklig/Hasklig-Bold.otf"), display.clone()).unwrap()
    });

    // Receiver from the thread being visualized.
    let visualizer_rx = RefCell::new(mpsc::sync_channel(0).1);

    let history = RefCell::new(vec![]);
    let position_stack = RefCell::new(vec![]);
    let current_position = Cell::new(0);

    let reverse = |dip| {
        let history = history.borrow();
        let mut position_stack = position_stack.borrow_mut();
        let mut i = current_position.get();

        let mut min_stack_size = position_stack.len();
        loop {
            // Don't do anything when we're at the start, just after
            // the first set of pushes. We also assume push/pop
            // pairs are properly matched, below.
            if i <= position_stack.len() { return; }

            // Reverse pushes.
            while let Action::Push(_) = history[i - 1] {
                i -= 1;
                position_stack.pop();
            }

            if min_stack_size > position_stack.len() {
                min_stack_size = position_stack.len();
            }

            // Reverse pops.
            while let Action::Pop(distance) = history[i - 1] {
                i -= 1;
                position_stack.push(i - distance);
            }

            // Leave the position between a push and a pop.
            current_position.set(i);

            if min_stack_size + dip <= position_stack.len() {
                break;
            }
        }
    };

    let advance = |dip| {
        let rx = visualizer_rx.borrow();
        let mut history = history.borrow_mut();
        let mut position_stack = position_stack.borrow_mut();
        let mut i = current_position.get();

        macro_rules! next {
            () => (*{
                while i >= history.len() {
                    match rx.recv() {
                        Ok(x) => history.push(x),
                        Err(_) => return
                    }
                }
                &history[i]
            })
        }

        let mut min_stack_size = position_stack.len();
        loop {
            // Apply pops.
            while let Action::Pop(_) = next!() {
                position_stack.pop();
                i += 1;
            }

            if min_stack_size > position_stack.len() {
                min_stack_size = position_stack.len();
            }

            // Apply pushes.
            while let Action::Push(_) = next!() {
                position_stack.push(i);
                i += 1;

                // Set the position between a push and a pop.
                // If no pushes are found, the position is unchanged.
                current_position.set(i);
            }

            if min_stack_size + dip <= position_stack.len() {
                break;
            }
        }
    };

    let press = |ui::event::KeyPress(key)| {
        match key {
            ui::event::Key::Left => { reverse(1); true }
            ui::event::Key::Right => { advance(1); true }
            ui::event::Key::PageUp => { reverse(2); true }
            ui::event::Key::PageDown => { advance(2); true }
            ui::event::Key::Home => { reverse(100); true }
            ui::event::Key::End => { advance(100); true }
            _ => false
       }
    };

    let tool_bar = tool_bar![
        ui::tool::Button::new("Open", || {
            if let Some(path) = ui::dialog::open_file() {
                let image = image::open(path).unwrap().to_luma();

                let (tx, rx) = mpsc::sync_channel(0);
                *visualizer_rx.borrow_mut() = rx;

                history.borrow_mut().clear();
                position_stack.borrow_mut().clear();
                current_position.set(0);

                thread::spawn(move || {
                    visualize::enter(tx, || {
                        medimgrs::barcode_2d::qr::find(&image);
                    });
                });

                // Move just before the first pop.
                advance(0);
            }
        }),
        ui::tool::Button::new("«", || reverse(2)),
        ui::tool::Button::new("< Previous", || reverse(1)),
        ui::tool::Button::new("Next >", || advance(1)),
        ui::tool::Button::new("»", || advance(2)),
    ];
    let mut root = flow![down: tool_bar, ui::empty::Empty::new()];

    let (mut x, mut y) = (0.0, 0.0);
    let mut key_tracker = ui::event::KeyTracker::default();
    let mut last_update = time::precise_time_ns();
    let mut cursor = ui::draw::MouseCursor::Default;

    // Ready the buffers.
    display.draw().finish().unwrap();
    let mut dirty = true;
    'main: loop {
        use glium::glutin::Event as E;
        use glium::glutin::{ElementState, MouseButton, MouseScrollDelta};

        for event in display.poll_events() {
            dirty |= match event {
                E::KeyboardInput(ElementState::Pressed, _, Some(key)) => {
                    let mut dirty = root.dispatch(&ui::event::KeyDown(key));
                    for e in key_tracker.down(key) {
                        dirty |= root.dispatch(&e);
                        dirty |= press(e);
                    }
                    dirty
                }
                E::KeyboardInput(ElementState::Released, _, Some(key)) => {
                    let mut dirty = root.dispatch(&ui::event::KeyUp(key));
                    for e in key_tracker.up(key) {
                        dirty |= root.dispatch(&e);
                        dirty |= press(e);
                    }
                    dirty
                }
                E::MouseInput(ElementState::Pressed, MouseButton::Left) => {
                    root.dispatch(&ui::event::MouseDown::new(x, y))
                }
                E::MouseInput(ElementState::Released, MouseButton::Left) => {
                    root.dispatch(&ui::event::MouseUp::new(x, y))
                }
                E::MouseMoved((nx, ny)) => {
                    x = nx as Px;
                    y = ny as Px;
                    root.dispatch(&ui::event::MouseMove::new(x, y))
                }
                // FIXME Convert lines into pixels, or vice-versa.
                E::MouseWheel(MouseScrollDelta::LineDelta(dx, dy)) |
                E::MouseWheel(MouseScrollDelta::PixelDelta(dx, dy)) => {
                    root.dispatch(&ui::event::MouseScroll::with(x, y,
                        ui::event::mouse::Scroll([dx as Px, dy as Px])))
                }
                E::ReceivedCharacter(c) => {
                    root.dispatch(&ui::event::TextInput(c))
                }
                E::Resized(w, h) => { println!("resized to {}x{}", w, h); true }
                E::Closed => break 'main,
                _ => false
            }
        }

        let current = time::precise_time_ns();
        let dt = (current - last_update) as f32 / 1e9;
        dirty |= root.dispatch(&ui::event::Update(dt));
        for e in key_tracker.update(dt) {
            dirty |= root.dispatch(&e);
            dirty |= press(e);
        }
        last_update = current;

        if dirty {
            let mut draw_cx = DrawCx::new(renderer, &display, display.draw());
            let [w, h] = draw_cx.dimensions();

            // TODO maybe integrate this with draw_cx?
            ui::layout::compute(&mut root, draw_cx.fonts(), w, h);

            draw_cx.clear(cfg::ColorScheme.background());
            draw_cx.draw(&root);

            {
                const SPACE: Px = 20.0;

                let font = ui::text::Regular::default();
                let history = history.borrow();
                let [sx, mut sy] = root.kids.1.bb().top_left();
                for &i in &position_stack.borrow()[..] {
                    if let Action::Push(ref row) = history[i] {
                        let [mut x, y] = [sx, sy];
                        for &(ref name, ref object) in row {
                            // Draw the title.
                            draw_cx.text(font, [x, y], cfg::ColorScheme.normal(), name);
                            let [x2, y2] = {
                                let fonts = draw_cx.fonts();
                                [x + fonts.text_width(font, name), y + fonts.metrics(font).height]
                            };

                            // Draw the object.
                            let [x3, y3] = object.visualize(&mut draw_cx, [x, y2 + SPACE / 2.0]);

                            x = x2.max(x3) + SPACE;
                            sy = sy.max(y3);
                        }
                    }
                    sy += SPACE;
                }
            }

            let new_cursor = draw_cx.get_cursor();
            draw_cx.finish();

            if (new_cursor as usize) != (cursor as usize) {
                if !cfg!(windows) {
                    display.get_window().map(|w| w.set_cursor(new_cursor));
                }
                cursor = new_cursor;
            }

            dirty = false;
        } else {
            // Sleep for half a frame (assuming 60FPS).
            std::thread::sleep_ms(1000 / 120);
        }
    }
}
