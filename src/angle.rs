use std::f64::consts::PI;
use std::mem;
use std::ops::Range;

#[derive(Copy, Clone, Default)]
pub struct Ratio {
    pub num: u32,
    pub div: u32
}

/// Orientations are stored as "double angles": where
/// the angle of an orientation would be in the range
/// [-PI/2, PI/2], the orientation falls in [-PI, PI],
/// where 0 is horizontal and -PI and PI are vertical.
/// This is so that similar orientations are close on
/// the unit circle, whereas orthogonal orientations
/// are diametrically opposed.
#[derive(Copy, Clone, Default)]
pub struct Orientation {
    double_angle: f64
}

impl Orientation {
    pub fn from_angle(angle: f64) -> Orientation {
        Orientation { double_angle: angle * 2.0 }
    }

    pub fn from_double_angle(angle: f64) -> Orientation {
        Orientation { double_angle: angle }
    }

    pub fn to_angle(self) -> f64 {
        self.double_angle / 2.0
    }

    pub fn to_double_angle(self) -> f64 {
        self.double_angle
    }

    /// Determine "cosine similarity" between the two orientations.
    /// That is, whether |cos(a - b)| > threshold, where a and b
    /// are the angles of the orientations, in [-PI/2, PI/2].
    /// The threshold is in the [0, 1] range, where 0 will
    /// always return true, and 1 will require the orientations
    /// to be identical.
    pub fn similar(self, other: Orientation, threshold: f64) -> bool {
        let (a, b) = (self.double_angle, other.double_angle);
        let delta = (a - b).abs();
        delta.min(2.0 * PI - delta) < threshold.acos() * 2.0
    }
}

#[derive(Copy, Clone, Default)]
pub struct OrientationAverage {
    sum_x: f64,
    sum_y: f64
}

impl OrientationAverage {
    /// Add the given orientation angle to the average.
    pub fn add(&mut self, orientation: Orientation) {
        // Use the double angle in the[-PI, PI] range.
        // This allows vertical orientations to be close
        // on the unit circle even if they're -PI and PI.
        let angle = orientation.to_double_angle();

        let (sin, cos) = (angle.sin(), angle.cos());
        self.sum_x += cos;
        self.sum_y += sin;
    }

    /// Compute the average orientation by averaging
    /// the positions of the doubled angles on the circle.
    /// The phase of the average position is used as
    /// the double angle of the resulting orientation.
    pub fn compute(&self) -> Orientation {
        Orientation::from_double_angle(self.sum_y.atan2(self.sum_x))
    }
}

pub struct BresenhamPoints {
    pub x: Range<i32>,
    pub y: i32,
    pub y_step: i32,
    pub steep: bool,
    pub reversed: bool,
    pub slope: f64,
    h: u32,
    /// Fractional part of the vertical sampling coordinate.
    y_frac: Ratio,
    sample: Range<usize>
}

/// Creates an iterator for the Bresenham oblique line approximation,
/// with `samples` number of points per long-side pixel.
/// The steep property indicates whether the line is steep, i.e.
/// the coordinates are (y, x), instead of (x, y).
/// Each sample is a (x, y, y_frac) triplet, where the center point is at
/// (x + 0.5, y + y_frac), inside the pixel at (x, y).
pub fn bresenham_points(from: [f64; 2], to: [f64; 2], samples: usize) -> BresenhamPoints {
    let ([x1, y1], [x2, y2]) = (from, to);

    let [mut x1, mut y1] = [x1.round() as i32, y1.round() as i32];
    let [mut x2, mut y2] = [x2.round() as i32, y2.round() as i32];

    let steep = (y2 - y1).abs() > (x2 - x1).abs();

    // Prefer the axis with less incline.
    if steep {
        mem::swap(&mut x1, &mut y1);
        mem::swap(&mut x2, &mut y2);
    }

    let reversed = x1 > x2;

    // Prefer left-to-right (or top-to-bottom).
    if reversed {
        mem::swap(&mut x1, &mut x2);
        mem::swap(&mut y1, &mut y2);
    }

    let w = (x2 - x1) as u32;
    let h = (y2 - y1).abs() as u32;

    BresenhamPoints {
        x: x1..x2,
        y: y1,
        y_step: if y1 < y2 { 1 } else { -1 },
        steep: steep,
        reversed: reversed,
        slope: ((h * h + w * w) as f64).sqrt() / w as f64,
        h: h,
        y_frac: Ratio {
            num: w * samples as u32 / 2,
            div: w * samples as u32
        },
        sample: 0..samples,
    }
}

impl Iterator for BresenhamPoints {
    type Item = (i32, i32, Ratio);

    fn next(&mut self) -> Option<Self::Item> {
        if self.x.start >= self.x.end {
            return None;
        }
        let x = self.x.start;
        let sample = self.sample.start;
        let x = if sample > self.sample.end / 2 { x + 1 } else { x };

        let y = self.y;
        let y_frac = self.y_frac;

        self.y_frac.num += self.h;
        if self.y_frac.num >= y_frac.div {
            self.y += self.y_step;
            self.y_frac.num -= self.y_frac.div;
        }

        self.sample.start += 1;
        if self.sample.start >= self.sample.end {
            self.x.next();
            self.sample.start = 0;
        }

        Some((x, y, y_frac))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let n = (self.x.end - self.x.start) as usize * self.sample.end - self.sample.start;
        (n, Some(n))
    }
}
