#![feature(associated_type_defaults, iter_arith, iter_cmp)]
#![feature(scoped_tls, slice_patterns, test)]

extern crate test;
extern crate image;

#[cfg(feature = "visualizer")]
#[macro_use]
pub mod visualize;

// If the visualizer is not enabled, fake the macro.
#[cfg(not(feature = "visualizer"))]
macro_rules! visualize {
    ($($name:expr => $value:expr),*) => {()}
}

pub mod angle;
pub mod barcode_1d;
pub mod chunk;
pub mod greyscale;
pub mod union_find;
pub mod barcode_2d;
