use std::f64::consts::PI;
use std::ops::{BitAnd, BitOr, Index, Not, Shl, Shr};

use image::{self, ImageBuffer};

use angle::Orientation;
use greyscale::{histogram, Luma, otsu_threshold};

pub const SIZE: usize = 32;
pub type Chunk<T: Pixel> = [T::Row; SIZE];
pub trait Pixel: Copy {
    type Row = [Self; SIZE];

    fn to_rgba(self) -> [f32; 4];
}

#[derive(Copy, Clone, PartialEq, Eq)]
pub struct BitRow(pub u32);

impl Pixel for bool {
    type Row = BitRow;

    fn to_rgba(self) -> [f32; 4] {
        if self {
            [0.0, 0.0, 0.0, 1.0]
        } else {
            [1.0, 1.0, 1.0, 1.0]
        }
    }
}

impl Index<usize> for BitRow {
    type Output = bool;
    fn index(&self, index: usize) -> &bool {
        const TRUE: &'static bool = &true;
        const FALSE: &'static bool = &false;
        if (self.0 >> index) & 1 == 1 {
            TRUE
        } else {
            FALSE
        }
    }
}

impl BitRow {
    pub fn set(&mut self, index: usize, bit: bool) {
        self.0 = self.0 & !( 1 << index ) | ((bit as u32) << index);
    }
}

impl<T> Shl<T> for BitRow where  u32: Shl<T, Output=u32> {
    type Output = BitRow;
    fn shl(self, count: T) -> BitRow {
        BitRow(self.0 << count)
    }
}

impl<T> Shr<T> for BitRow where  u32: Shr<T, Output=u32> {
    type Output = BitRow;
    fn shr(self, count: T) -> BitRow {
        BitRow(self.0 >> count)
    }
}

impl BitAnd for BitRow {
    type Output = BitRow;
    fn bitand(self, other: BitRow) -> BitRow {
        BitRow(self.0 & other.0)
    }
}
impl BitOr for BitRow {
    type Output = BitRow;
    fn bitor(self, other: BitRow) -> BitRow {
        BitRow(self.0 | other.0)
    }
}

impl Not for BitRow {
    type Output = BitRow;
    fn not(self) -> BitRow {
        BitRow(!self.0)
    }
}

pub fn otsu_chunks(pixels: &[Luma], width: usize) -> Vec<Chunk<bool>> {
    let threshold = otsu_threshold(&histogram(pixels), pixels.len());
    let height = pixels.len() / width;

    let chunks_width = width / SIZE;
    let chunks_height = height / SIZE;
    let mut chunks: Vec<_> = (0..chunks_width * chunks_height).map(|_| [BitRow(0); SIZE]).collect();
    let start_row = (height % SIZE) / 2;
    let start_column = (width % SIZE) / 2;
    for row in 0..chunks_height {
        for i in 1..SIZE-1 {
            for column in 0..chunks_width {
                let mut chunk_row = BitRow(0);
                for j in 1..SIZE-1 {
                    let px = pixels[(start_row + row * SIZE + i) * width +
                                    (start_column + column * SIZE + j)] > threshold;
                    chunk_row.set(j, px);
                }
                chunks[row * chunks_width + column][i] = chunk_row;
            }
        }
    }
    chunks
}

pub fn skeletonize_chunk(mut chunk: Chunk<bool>) -> Chunk<bool> {
    let mut skel = [BitRow(0); SIZE];
    loop {
        let mut eroded = [BitRow(0); SIZE];
        let mut dilated = [BitRow(0); SIZE];
        let mut pixels_left = false;

        // Erode the current chunk further.
        for i in 1..SIZE-1 {
            let (above, below) = (chunk[i - 1], chunk[i + 1]);
            eroded[i] = chunk[i] & (above << 1) & (above >> 1) & (below << 1) & (below >> 1);
            pixels_left |= eroded[i] != BitRow(0);
        }

        // Dilate the eroded chunk.
        for i in 1..SIZE-1 {
            let (above, below) = (eroded[i - 1], eroded[i + 1]);
            dilated[i] = eroded[i] | (above << 1) | (above >> 1) | (below << 1) | (below >> 1);
        }

        // Add the difference between the chunk and the dilated erosion to the skeleton.
        for i in 1..SIZE-1 {
            skel[i] = skel[i] | chunk[i] & !dilated[i];
        }

        visualize! {
            "Skeleton" => skel,
            "Eroded" => eroded,
            "Dilated" => dilated
        };

        // Continue with the eroded chunk.
        if !pixels_left {
            break;
        }
        chunk = eroded;
    }
    skel
}

pub fn chunks_to_image(chunks: &[Chunk<bool>], width: usize)
                       -> ImageBuffer<image::Luma<u8>, Vec<u8>> {
    let mut pixels: Vec<u8> = (0..chunks.len() * SIZE * SIZE).map(|_| 0).collect();

    let height = chunks.len() / width;
    for row in 0..height {
        for column in 0..width {
            let chunk = &chunks[row * width + column];
            for i in 0..SIZE {
                for j in 0..SIZE {
                    let px = if chunk[i][j] { 0x00 } else { 0xff };
                    pixels[((row * SIZE + i) * width + column) * SIZE + j] = px;
                }
            }
        }
    }

    ImageBuffer::from_vec((width * SIZE) as u32, (height * SIZE) as u32, pixels).unwrap()
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Label(pub u8);

impl Pixel for Label {
    fn to_rgba(self) -> [f32; 4] {
        if self == Label(0) {
            return [1.0, 1.0, 1.0, 1.0];
        }
        if self == CONTOUR_HALO {
            return [0.9, 0.9, 0.8, 1.0];
        }

        // Reverse the bits of the label.
        let mut bits = self.0 - 1;
        let mut rev = 0;
        for _ in 0..8 {
            rev = (rev << 1) | (bits & 1);
            bits >>= 1;
        }

        let hue = (rev as f32 / 256.0) * 6.0;
        let c = 1.0 - (hue % 2.0 - 1.0).abs();

        // Convert the hue to RGBA.
        if hue < 1.0 {
            [1.0, c, 0.0, 1.0]
        } else if hue < 2.0 {
            [c, 1.0, 0.0, 1.0]
        } else if hue < 3.0 {
            [0.0, 1.0, c, 1.0]
        } else if hue < 4.0 {
            [0.0, c, 1.0, 1.0]
        } else if hue < 5.0 {
            [c, 0.0, 1.0, 1.0]
        } else if hue < 6.0 {
            [1.0, 0.0, c, 1.0]
        } else {
            unreachable!()
        }
    }
}

pub const CONTOUR_HALO: Label = Label(255);
pub const DIRECTIONS: [[isize; 2]; 8] = [
    [0, 1], [1, 1], [1, 0], [1, -1],
    [0, -1], [-1, -1], [-1, 0], [-1, 1]
];

pub fn tracer(chunk: &Chunk<bool>,
              x: &mut usize,
              y: &mut usize,
              dir: &mut usize,
              labels: &mut Chunk<Label>) -> bool {
    for _ in 0..8 {
        let [dy, dx] = DIRECTIONS[*dir];
        let ny = (*y as isize + dy) as usize;
        let nx = (*x as isize + dx) as usize;
        if chunk[ny][nx] {
            labels[ny][nx] = labels[*y][*x];
            *x = nx;
            *y = ny;
            return true;
        } else {
            *dir = (*dir + 1) % 8;
            labels[ny][nx] = CONTOUR_HALO;
        }
    }
    false
}

pub fn contour_tracing(chunk: &Chunk<bool>,
                       labels: &mut Chunk<Label>,
                       sx: usize,
                       sy: usize,
                       mut dir: usize) -> bool {
    let mut x = sx;
    let mut y = sy;
    if tracer(chunk, &mut x, &mut y, &mut dir, labels) {
        let dir0 = dir;
        loop {
            let at_start = [x, y] == [sx, sy];
            dir = (dir + 6) % 8;
            tracer(chunk, &mut x, &mut y, &mut dir, labels);
            if at_start && (dir == dir0) {
                break;
            }
        }
        true
    } else {
        false
    }
}

#[test]
fn test_contour_tracing() {
    let label = Label(42);
    let mut chunk = [BitRow(0); SIZE];
    let mut labels = [[Label(0); SIZE]; SIZE];
    chunk[1].0 = 0b000010000;
    chunk[2].0 = 0b000101000;
    chunk[3].0 = 0b001101100;
    chunk[4].0 = 0b010010010;
    chunk[5].0 = 0b001101100;
    chunk[6].0 = 0b000101000;
    chunk[7].0 = 0b000010000;

    for &[x, y] in &[[1, 4], [2, 3], [6, 3], [3, 6]] {
        labels[y][x] = label;
        assert!(contour_tracing(&chunk, &mut labels, x, y, 3));
    }
    let labels2 = label_chunk(&chunk);
    for y in 0..9 {
       for x in 0..9 {
           print!(" {:02X}", labels2[y][x].0);
       }
       println!("");
    }
    for y in 0..SIZE {
        for x in 0..SIZE {
            if chunk[y][x] {
                assert_eq!(labels[y][x], label);
                assert_eq!(labels2[y][x], Label(1));
            } else {
                assert!(labels[y][x] == Label(0) ||
                        labels[y][x] == CONTOUR_HALO);
                assert!(labels2[y][x] == Label(0));
            }
        }
    }
}

pub fn label_chunk(chunk: &Chunk<bool>) -> Chunk<Label> {
    let mut labels = [[Label(0); SIZE]; SIZE];
    let mut counter = 1;
    for y in 1..SIZE-1 {
        let mut changed = false;
        for x in 1..SIZE-1 {
            if chunk[y][x] {
                if !chunk[y - 1][x] && labels[y][x] == Label(0) {
                    labels[y][x] = Label(counter);
                    if !contour_tracing(chunk, &mut labels, x, y, 7) {
                        labels[y][x] = Label(0);
                    } else {
                        visualize! {
                            "Chunk" => *chunk,
                            format!("After labeling #{}", counter) => labels
                        };
                        counter += 1;
                    }
                    changed = true;
                } else if labels[y][x] == Label(0) {
                    labels[y][x] = labels[y][x - 1];
                    changed = true;
                }
                if !chunk[y + 1][x] && labels[y + 1][x] == Label(0) {
                    contour_tracing(chunk, &mut labels, x, y, 3);
                    changed = true;
                    visualize! {
                        "Chunk" => *chunk,
                        format!("After tracing interior of #{}", labels[y][x].0) => labels
                    };
                }
            }
        }
        if changed {
            visualize! {
                "Chunk" => *chunk,
                format!("After labeling row #{}", y) => labels
            };
        }
    }
    for y in 0..SIZE {
        for x in 0..SIZE {
            if labels[y][x] == CONTOUR_HALO {
                labels[y][x] = Label(0);
            }
        }
    }
    visualize! {
        "Chunk" => *chunk,
        "Final labels" => labels
    };
    labels
}

#[test]
fn test_worst_case() {
    let pattern = BitRow(0b01010101_01010101_01010101_01010100);
    let mut chunk = [BitRow(0); SIZE];
    for i in 0..10 {
        chunk[i * 3 + 1] = pattern;
        chunk[i * 3 + 2] = pattern;
    }
    let labels = label_chunk(&chunk);
    for y in 0..SIZE {
        for x in 0..SIZE {
            assert!(labels[y][x].0 < 170);
        }
    }
}

#[derive(Copy, Clone, Default)]
pub struct Moments {
    m00: usize,
    m01: usize,
    m10: usize,
    m11: usize,
    m02: usize,
    m20: usize,
}

pub fn component_moments(labels: &Chunk<Label>) -> [Moments; 256] {
    let mut moments = [Moments::default(); 256];
    for y in 1..SIZE-1 {
        for x in 1..SIZE-1 {
            let c = &mut moments[labels[y][x].0 as usize];
            c.m00 += 1;
            c.m01 += y;
            c.m10 += x;
            c.m11 += x * y;
            c.m02 += y * y;
            c.m20 += x * x;
        }
    }
    visualize! {
        "Labels" => *labels,
        "Moments" => moments
    };
    moments
}

impl Moments {
    pub fn count(&self) -> usize {
        self.m00
    }

    pub fn orientation(self) -> Orientation {
        let m00 = self.m00 as f64;
        let x = self.m10 as f64 / m00;
        let y = self.m01 as f64 / m00;
        let u11 = self.m11 as f64 / m00 - x * y;
        let u02 = self.m02 as f64 / m00 - y * y;
        let u20 = self.m20 as f64 / m00 - x * x;
        let atan = ((u02 - u20) / (2.0 * u11)).atan();

        // Convert the angle to [-PI, PI] for an orientation.
        let angle = if u11 >= 0.0 {
            atan + PI / 2.0
        } else {
            atan - PI / 2.0
        };
        Orientation::from_double_angle(angle)
    }
}
