use angle::bresenham_points;
use barcode_1d::decode::{Code, Line};
use greyscale::otsu_threshold;

use image::{self, ImageBuffer};

use std::mem;
use std::f64::consts::PI;

#[derive(Default)]
struct Finder;

impl Code for Finder {
    fn name() -> &'static str { "QR" }
    fn unit_width() -> u32 { 7 }
    fn check_bar_error(error: u32, div: u32) -> bool {
        3 * error <= div
    }
    fn check_unit_error(error: u32, div: u32) -> bool {
    	true
    }
}

const FINDER_PATTERN: &'static [[u32; 5]] = &[[1, 1, 3, 1, 1]];

pub fn find(image: &ImageBuffer<image::Luma<u8>, Vec<u8>>) -> Vec<String> {
    let mut finders: Vec<[f64; 2]> = vec![];
    let mut max_length = 0.0;
    for y in 0..image.height() {
        let y = y as f64;
        let mut line = Line::sample(image, [0.0, y], [image.width() as f64, y]);
        let mut start = line.next(0, true);
        //let _v = visualize! { "Probing for finder patterns" => line.clone() };

        'line: while let Ok((mut next, _)) = line.find_patterns(start, Finder, FINDER_PATTERN) {
            start = next + 1;
            let x = line.range_width(0..next - 5);
            let length = line.range_width(next - 5..next);

            let mut line2 = Line::sample(image, [x + length / 2.0, y - length], [x + length / 2.0, y + length]);
            let next2 = line2.next(0, true);
            //let _v = visualize! { "Probing vertically" => line2.clone() };

            if let Ok((mut next2, _)) = line2.find_patterns(next2, Finder, FINDER_PATTERN) {
                let mid_y = line2.range_width(0..next2 - 3) + y - length;
                let mid_len = line2.width(next2 - 3);
                if (y - (mid_y + mid_len / 2.0)).abs() > mid_len / 4.0 {
                    continue 'line;
                }
                let cx = x + length / 2.0;
                let cy = mid_y + mid_len/ 2.0;
                for &[fx, fy] in &finders {
                    if (fx - cx).abs() < length && (fy - cy).abs() < length {
                        continue 'line;
                    }
                }
                finders.push([cx, cy]);
                if max_length < length {
                    max_length = length;
                }
                visualize! {
                    "Finder pattern candidate"
                    => Stacked(ImageLuma8(image.clone()),
                    Stacked(Line([x, y], [x + length, y], [1.0, 0.0, 0.0, 1.0]),
                            Line([x + length/2.0, mid_y], [x + length/2.0, mid_y + mid_len],
                                 [0.0, 1.0, 0.0, 1.0])))
                };
            }
        }
    }
    if finders.len() != 3 { return vec![];}
    let a = finders[0];
    let b = finders[1];
    let c = finders[2];


    let ab = distance(a, b);
    let ac = distance(a, c);
    let bc = distance(b, c);
    let (a, mut b, mut c) = if ab > ac && ab > bc {
        (c, b, a)
    } else if ac > bc {
        (b, a, c)
    } else {
        (a, b, c)
    };
    let mut angle_b = angle(b, a);
    let mut angle_c = angle(c, a);
    if ((angle_b - angle_c).abs() < PI) == (angle_b > angle_c) {
        mem::swap(&mut b, &mut c);
        mem::swap(&mut angle_b, &mut angle_c);
    }
    visualize! {
        "Triangle"
        => Stacked(ImageLuma8(image.clone()),
        vec![Line(a, b, [1.0, 0.0, 0.0, 1.0]),
             Line(b, c, [0.0, 1.0, 0.0, 1.0]),
             Line(a, c, [0.0, 0.0, 1.0, 1.0])])
    };
    let finder_edge = |[x, y]: [f64; 2], dir: f64| {
        let line = Line::sample(image, [x, y],
            [x + max_length * dir.cos(), y + max_length * dir.sin()]);

        let contour_start = line.range_width(0..2);
        let contour_width = line.width(2);
        let contour_center = contour_start + contour_width / 2.0;
        [x + contour_center * dir.cos(), y + contour_center * dir.sin()]
    };
    let horiz_left = finder_edge(a, angle_c);
    let horiz_right = finder_edge(b, angle_b + PI/2.0);
    let vert_top = finder_edge(a, angle_b);
    let vert_bottom = finder_edge(c, angle_c - PI/2.0);

    visualize! {
        "More Red Lines"
        => Stacked(ImageLuma8(image.clone()),
        Stacked(Line(horiz_left, horiz_right, [1.0, 0.0, 0.0, 1.0]),
                Line(vert_top, vert_bottom, [1.0, 0.0, 0.0, 1.0])))
    };

    let pixel_value = |x, y| {
        if x >= 0 && y >= 0 {
            let (x, y) = (x as u32, y as u32);
            if x < image.width() && y < image.height() {
                return Some(image[(x, y)].data[0]);
            }
        }
        None
    };

    let hist_points = bresenham_points(horiz_left, horiz_right, 1)
               .chain(bresenham_points(vert_top, vert_bottom, 1));
    let mut histogram = [0; 256];
    let mut hist_count = 0;
    for (x, y, _) in hist_points {
        if let Some(v) = pixel_value(x, y) {
            histogram[v as usize] += 1;
            hist_count += 1;
        }
    }
    let threshold = otsu_threshold(&histogram, hist_count);

    let timing_params = |from, to| {
        let line = Line::sample(image, from, to);
        let u = angle(to, from);
        let (a, b) = regression((1..line.len() - 2).map(|i| line.range_width(i..i + 2) / 2.0));
        let (a, b) = (a - 7.0 * b, b);
        let l = line.range_width(0..1) - (b * 7.0 * 7.0 / 2.0 - b * 7.0 / 2.0 + a * 7.0);
        let p = [from[0] + l * u.cos(), from[1] + l * u.sin()];
        let n = line.len() - 2 + 14;
        ((a, b, p, u), n)
    };
    let (h_timing, hn) = timing_params(horiz_left, horiz_right);
    let (v_timing, vn) = timing_params(vert_top, vert_bottom);
    if vn != hn {
        return vec![];
    }
    let n = vn;
    let timing_pos = |(a, b, [px, py], u): (f64, f64, [f64; 2], f64), n| {
        let l = (b / 2.0) * (n * n) + a * n + a / 2.0;
        ([px + l * u.cos(), py + l * u.sin()], u)
    };

    let point = |x, y| {
        let ([xh, yh], h) = timing_pos(h_timing, x);
        let ([xv, yv], v) = timing_pos(v_timing, y);
        let h = h + PI / 2.0;
        let v = v - PI / 2.0;
        let (sh, ch) = (h.sin(), h.cos());
        let (sv, cv) = (v.sin(), v.cos());
        let x = ((yv - yh) * ch * cv - xv * sv * ch + xh * sh * cv) / (h - v).sin();
        let (th, tv) = (h.tan(), v.tan());
        let y = if th.abs() < tv.abs() {
            yh + (x - xh) * th
        } else {
            yv + (x - xv) * tv
        };
        [x, y]
    };

    visualize! {
        "Points"
        => Stacked(ImageLuma8(image.clone()),
        (0..n).flat_map(|y| (0..n).map(move |x| (x, y))).map(|(x, y)| {
            let [px, py] = point(x as f64, y as f64);
            let v = pixel_value(px.round() as i32, py.round() as i32).unwrap_or(255);
            let v = v < threshold;
            Line([px - 1.0, py - 1.0], [px + 1.0, py - 1.0],
                 if v { [0.0, 1.0, 0.0, 1.0] } else { [0.0, 0.0, 1.0, 1.0] })
        }).collect::<Vec<_>>())
    };
    let cell_value = |[x, y]: [usize; 2]| {
        let [px, py] = point(x as f64, y as f64);
        let value = pixel_value(px.round() as i32, py.round() as i32).unwrap_or(255);
        value < threshold
    };
    if n < 21 || (n - 17) % 4 != 0 {
        return vec![];
    }
    let version = (n - 17) / 4;
    let format1 = (n - 8..n).rev().map(|x| [x, 8])
           .chain((n - 7..n).map(|y| [8, y]))
           .enumerate().fold(0, |v, (i, p)| v | ((cell_value(p) as u32) << i));
    println!("{:015b}", format1);
    let format2 = (0..6).map(|y| [8, y])
            .chain([[8, 7], [8, 8], [7, 8]].iter().cloned())
            .chain((0..6).rev().map(|x| [x, 8]))
            .enumerate().fold(0, |v, (i, p)| v | ((cell_value(p) as u32) << i));
    println!("{:015b}", format2);
    if format1 != format2 {
        return vec![];
    }
    let format = format1 ^ 0x5412;
    let mask_type = (format >> 10) & 7;
    let align_spacing = (version * 4 + version / 7 + 4) / (version / 7 + 1);
    let is_occupied = |[x, y]: [usize; 2]| {
        x <= 8 && y <= 8 || x >= n-8 && y <= 8 || x <= 8 && y >= n - 8 ||
        y == 6 || x == 6 ||
        5 <= x && x <= n - 6 && ((((n - 6 - x) % align_spacing) <= 2) || x <= 7) &&
        5 <= y && y <= n - 6 && ((((n - 6 - y) % align_spacing) <= 2) || y <= 7)
    };
    let next_data_cell = |[x, y]: [usize; 2]| -> [usize; 2] {
        let lane = if x > 6 { x - 1 } else { x } % 4;
        if lane == 0 && y != n - 1 {
            [x + 1, y + 1]
        } else if lane == 2 && y != 0 {
                [x + 1, y - 1]
        } else {
            [if x == 7 { 5 } else { x - 1 }, y]
        }

    };
    let next_valid_data_cell = |mut p| {
        loop {
            p = next_data_cell(p);
            if !is_occupied(p) {
                return p;
            }
        }
    };

    let mut raw_data:Vec<u8> = vec![];
    let mut current_cell = [n-1,  n-1];
'outer:    loop {
        let mut b = 0;
        for i in 0..8 {
            let [x, y] = current_cell;
            let mask = match mask_type {
                0b000 => (x + y) % 2 == 0,
                0b001 => y % 2 == 0,
                0b010 => x % 3 == 0,
                0b011 => (x + y) % 3 == 0,
                0b100 => (y % 2 + x % 3) % 2 == 0,
                0b101 => (x * y) % 2 + (x * y) % 3 == 0,
                0b110 => ((x * y) % 2 + (x * y) % 3) % 2 == 0,
                0b111 => ((x + y) % 2 + (x * y) % 3) % 2 == 0,
                _ => unreachable!()
            };
            b |= ((cell_value(current_cell) ^ mask) as u8) << (7 - i);
            if current_cell == [0, n - 9] {
                raw_data.push(b);
                break 'outer;
            }
            current_cell = next_valid_data_cell(current_cell);
        }
        raw_data.push(b);
    }
    let mut byte_pos = 0;
    let mut bit_pos = 8;

    // Read n bits (at most 32) into an u32 value.
    let mut data_bits = |mut n: usize| {
        let mut v = 0;
        loop {
            if bit_pos == 0 {
                byte_pos += 1;
                bit_pos = 8;
            }
            let b = raw_data[byte_pos] as u32;
            if n < bit_pos {
                bit_pos -= n;
                return v | (b >> bit_pos) & ((1 << n) - 1);
            }
            n -= bit_pos;
            v |= (b & ((1 << bit_pos) - 1)) << n;
            bit_pos = 0;
        }
    };

    let mode = data_bits(4);

    match mode {
        // Numeric mode (3 digits every 10 bits).
        0b0001 => {
            let len = data_bits(match version {
                1...9 => 10, 10...26 => 12, 27...40 => 14,
                _ => unreachable!()
            });
            let mut s = String::with_capacity(len as usize);
            {
                let mut digit = |x| s.push((b'0' + (x % 10) as u8) as char);
                for _ in 0..len / 3 {
                    let v = data_bits(10);
                    digit(v / 100);
                    digit(v / 10);
                    digit(v);
                }
                if len % 3 >= 1 {
                    let v = data_bits(10);
                    digit(v / 100);
                    if len % 3 == 2 {
                        digit(v / 10);
                    }
                }
            }
            vec![s]
        }

        // Alphanumeric mode (2 chars every 11 bits).
        0b0010 => {
            let len = data_bits(match version {
                1...9 => 9, 10...26 => 11, 27...40 => 13,
                _ => unreachable!()
            });
            let mut s = String::with_capacity(len as usize);
            {
                let mut put_char = |x| {
                    s.push(match x {
                        0...9 => (b'0' + x as u8) as char,
                        10...35 => (b'A' + (x - 10) as u8) as char,
                        36...44 => b" $%*+-./:"[(x - 36) as usize] as char,
                        _ => '\u{FFFD}'
                    });
                };
                for _ in 0..len / 2 {
                    let v = data_bits(11);
                    put_char(v / 45);
                    put_char(v % 45);
                }
                if len % 2 == 1 {
                    put_char(data_bits(11) / 45);
                }
            }
            vec![s]
        }

        // Byte mode.
        0b0100 => {
            let len = data_bits(if version < 10 { 8 } else { 16 });
            let data: Vec<u8> = (0..len).map(|_| data_bits(8) as u8).collect();
            vec![String::from_utf8_lossy(&data).into_owned()]
        }

        // Invalid/unhandled mode.
        _ => vec![]
        }
}

fn distance([ax, ay]: [f64; 2], [bx, by]: [f64; 2]) -> f64 {
    ((ax - bx) * (ax - bx) + (ay - by) * (ay - by)).sqrt()
}

fn angle([ax, ay]: [f64; 2], [bx, by]: [f64; 2]) -> f64 {
    (ay - by).atan2(ax - bx)
}

fn regression<I: Iterator<Item=f64>>(v: I) -> (f64, f64) {
    let mut sum_y = 0.0;
    let mut sum_product = 0.0;
    let mut x = 0.0;
    for y in v {
        sum_y += y;
        sum_product += x * y;
        x = x + 1.0;
    }
    let n = x;
    let sum_x = n * (n - 1.0) / 2.0;
    let sum_squares = n * (n - 1.0) * (2.0 * n - 1.0) / 6.0;
    let b = (sum_product - (sum_x * sum_y) / n) / (sum_squares - (sum_x * sum_x) / n);
    let a = sum_y / n - b * (sum_x / n);
    (a,b)
}
