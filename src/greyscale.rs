pub type Luma = u8;
pub type LumaHistogram = [usize; 256];

pub fn histogram(pixels: &[Luma]) -> LumaHistogram {
    let mut histogram = [0; 256];
    for &px in pixels {
        histogram[px as usize] += 1;
    }
    histogram
}

pub fn otsu_threshold(histogram: &LumaHistogram, total: usize) -> Luma {
    let total = total as u64;
    let sum: u64 = histogram.iter().enumerate().map(|(i, &h)| (i * h) as u64).sum();
    let (mut sum_b, mut w_b) = (0, 0);
    let (mut max, mut t1, mut t2) = (0, 0, 0);
    for (i, &h) in histogram.iter().enumerate() {
        sum_b += (i * h) as u64;
        w_b += h as u64;

        // Prevent division by 0.
        if w_b == 0 { continue; }
        if w_b == total { break; }

        let w_f = total - w_b;
        let mu_b = sum_b / w_b;
        let mu_f = (sum - sum_b) / w_f;

        let mu_diff = if mu_b > mu_f { mu_b - mu_f } else { mu_f - mu_b };
        let between = w_b * w_f * mu_diff * mu_diff;
        if between >= max {
            t1 = i;
            if between > max {
                t2 = i;
            }
            max = between;
        }
    }

    ((t1 as u16 + t2 as u16) / 2) as Luma
}
