use barcode_1d::decode::{Code, Line};

#[derive(Default)]
struct Start;

#[derive(Default)]
struct Payload;

const CODE_BARS: usize = 6;
const CODE_WIDTH: u32 = 11;

impl Code for Start {
    fn name() -> &'static str { "Code128 (start)" }
    fn unit_width() -> u32 { CODE_WIDTH }
}
impl Code for Payload {
    fn name() -> &'static str { "Code128" }
    fn unit_width() -> u32 { CODE_WIDTH }
    fn check_unit_error(error: u32, div: u32) -> bool { true }
}

const CODE_SHIFT: u8 = 98;
const CODE_C: u8 = 99;
const CODE_B: u8 = 100;
const CODE_A: u8 = 101;
const START_CODE_A: u8 = 103;
const START_CODE_B: u8 = 104;
const START_CODE_C: u8 = 105;
const STOP_CODE: u8 = 106;
const CODE_PATTERN: &'static [[u32; CODE_BARS]] = &[
    [2, 1, 2, 2, 2, 2],
    [2, 2, 2, 1, 2, 2],
    [2, 2, 2, 2, 2, 1],
    [1, 2, 1, 2, 2, 3],
    [1, 2, 1, 3, 2, 2],
    [1, 3, 1, 2, 2, 2],
    [1, 2, 2, 2, 1, 3],
    [1, 2, 2, 3, 1, 2],
    [1, 3, 2, 2, 1, 2],
    [2, 2, 1, 2, 1, 3],
    [2, 2, 1, 3, 1, 2],
    [2, 3, 1, 2, 1, 2],
    [1, 1, 2, 2, 3, 2],
    [1, 2, 2, 1, 3, 2],
    [1, 2, 2, 2, 3, 1],
    [1, 1, 3, 2, 2, 2],
    [1, 2, 3, 1, 2, 2],
    [1, 2, 3, 2, 2, 1],
    [2, 2, 3, 2, 1, 1],
    [2, 2, 1, 1, 3, 2],
    [2, 2, 1, 2, 3, 1],
    [2, 1, 3, 2, 1, 2],
    [2, 2, 3, 1, 1, 2],
    [3, 1, 2, 1, 3, 1],
    [3, 1, 1, 2, 2, 2],
    [3, 2, 1, 1, 2, 2],
    [3, 2, 1, 2, 2, 1],
    [3, 1, 2, 2, 1, 2],
    [3, 2, 2, 1, 1, 2],
    [3, 2, 2, 2, 1, 1],
    [2, 1, 2, 1, 2, 3],
    [2, 1, 2, 3, 2, 1],
    [2, 3, 2, 1, 2, 1],
    [1, 1, 1, 3, 2, 3],
    [1, 3, 1, 1, 2, 3],
    [1, 3, 1, 3, 2, 1],
    [1, 1, 2, 3, 1, 3],
    [1, 3, 2, 1, 1, 3],
    [1, 3, 2, 3, 1, 1],
    [2, 1, 1, 3, 1, 3],
    [2, 3, 1, 1, 1, 3],
    [2, 3, 1, 3, 1, 1],
    [1, 1, 2, 1, 3, 3],
    [1, 1, 2, 3, 3, 1],
    [1, 3, 2, 1, 3, 1],
    [1, 1, 3, 1, 2, 3],
    [1, 1, 3, 3, 2, 1],
    [1, 3, 3, 1, 2, 1],
    [3, 1, 3, 1, 2, 1],
    [2, 1, 1, 3, 3, 1],
    [2, 3, 1, 1, 3, 1],
    [2, 1, 3, 1, 1, 3],
    [2, 1, 3, 3, 1, 1],
    [2, 1, 3, 1, 3, 1],
    [3, 1, 1, 1, 2, 3],
    [3, 1, 1, 3, 2, 1],
    [3, 3, 1, 1, 2, 1],
    [3, 1, 2, 1, 1, 3],
    [3, 1, 2, 3, 1, 1],
    [3, 3, 2, 1, 1, 1],
    [3, 1, 4, 1, 1, 1],
    [2, 2, 1, 4, 1, 1],
    [4, 3, 1, 1, 1, 1],
    [1, 1, 1, 2, 2, 4],
    [1, 1, 1, 4, 2, 2],
    [1, 2, 1, 1, 2, 4],
    [1, 2, 1, 4, 2, 1],
    [1, 4, 1, 1, 2, 2],
    [1, 4, 1, 2, 2, 1],
    [1, 1, 2, 2, 1, 4],
    [1, 1, 2, 4, 1, 2],
    [1, 2, 2, 1, 1, 4],
    [1, 2, 2, 4, 1, 1],
    [1, 4, 2, 1, 1, 2],
    [1, 4, 2, 2, 1, 1],
    [2, 4, 1, 2, 1, 1],
    [2, 2, 1, 1, 1, 4],
    [4, 1, 3, 1, 1, 1],
    [2, 4, 1, 1, 1, 2],
    [1, 3, 4, 1, 1, 1],
    [1, 1, 1, 2, 4, 2],
    [1, 2, 1, 1, 4, 2],
    [1, 2, 1, 2, 4, 1],
    [1, 1, 4, 2, 1, 2],
    [1, 2, 4, 1, 1, 2],
    [1, 2, 4, 2, 1, 1],
    [4, 1, 1, 2, 1, 2],
    [4, 2, 1, 1, 1, 2],
    [4, 2, 1, 2, 1, 1],
    [2, 1, 2, 1, 4, 1],
    [2, 1, 4, 1, 2, 1],
    [4, 1, 2, 1, 2, 1],
    [1, 1, 1, 1, 4, 3],
    [1, 1, 1, 3, 4, 1],
    [1, 3, 1, 1, 4, 1],
    [1, 1, 4, 1, 1, 3],
    [1, 1, 4, 3, 1, 1],
    [4, 1, 1, 1, 1, 3],
    [4, 1, 1, 3, 1, 1],
    [1, 1, 3, 1, 4, 1],
    [1, 1, 4, 1, 3, 1],
    [3, 1, 1, 1, 4, 1],
    [4, 1, 1, 1, 3, 1],
    [2, 1, 1, 4, 1, 2],
    [2, 1, 1, 2, 1, 4],
    [2, 1, 1, 2, 3, 2],
    // TODO(eddyb) figure out what the "2" at the end is doing.
    [2, 3, 3, 1, 1, 1/*, 2*/]
];

pub fn decode(line: &Line) -> Result<String, ()> {
    // Find the start code.
    let start_patterns = &CODE_PATTERN[(START_CODE_A as usize)..(START_CODE_C as usize+1)];
    let start = line.next(0, true);
    let (mut start, code) = try!(line.find_patterns(start, Start, start_patterns));
    let mut code = code as u8 + START_CODE_A;
    let mut codeset = match code {
        START_CODE_A => CODE_A,
        START_CODE_B => CODE_B,
        START_CODE_C => CODE_C,
        _ => return Err(())
    };

    let mut checksum = code as u32;
    let mut multiplier = 0;
    let mut result = String::new();
    let mut last_result_len = 0;

    // Decode every code in the barcode.
    loop {
        let unshift = code == CODE_SHIFT;

        let new_code = try!(line.match_patterns(start, Payload, CODE_PATTERN));

        if new_code as u8 == STOP_CODE {
            break;
        }

        start += CODE_BARS;

        code = new_code as u8;
        last_result_len = result.len();

        multiplier += 1;
        checksum += multiplier * code as u32;

        match codeset {
            CODE_A => match code {
                0...63 => result.push((code + 32) as char),
                64...95 => result.push((code - 64) as char),
                _ => {}
            },
            CODE_B => if code < 96{
                result.push((code + 32) as char);
            },
            CODE_C => if code < 100 {
                result.push((b'0' + code / 10) as char);
                result.push((b'0' + code % 10) as char);
            },
            _ => {}
        }

        match code {
            CODE_A | CODE_B | CODE_C => codeset = code,
            _ => {}
        }

        // Switch between CODE_A and CODE_B.
        codeset ^= ((code == CODE_SHIFT) ^ unshift) as u8;
    }

    // Include the stop bar at the end of the STOP_CODE.
    let end = start + CODE_BARS + 1;

    // Ensure trailing whitespace is present.
    let white_count = line.range_count(start..end) / 2;
    if line.count(end) < white_count {
        return Err(());
    }

    // Verify checksum.
    if ((checksum - multiplier * code as u32) % 103) as u8 != code {
        return Err(());
    }

    // Trim away the checksum, if it ended up in the result.
    result.truncate(last_result_len);

    Ok(result)
}
