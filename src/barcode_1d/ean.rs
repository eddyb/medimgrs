use barcode_1d::decode::{Code, Line, };

#[derive(Default)]
struct Start;

#[derive(Default)]
struct Middle;

#[derive(Default)]
struct Payload;

const CODE_BARS: usize = 4;
const CODE_WIDTH: u32 = 7;

impl Code for Start {
    fn name() -> &'static str { "EAN (start/stop)" }
    fn unit_width() -> u32 { 3 }
    fn check_bar_error(error: u32, div: u32) -> bool {
        Payload::check_bar_error(error, div)
    }
    fn check_unit_error(error: u32, div: u32) -> bool {
        Payload::check_unit_error(error, div)
    }
}
impl Code for Middle {
    fn name() -> &'static str { "EAN (middle)" }
    fn unit_width() -> u32 { 5 }
    fn check_bar_error(error: u32, div: u32) -> bool {
        Payload::check_bar_error(error, div)
    }
    fn check_unit_error(error: u32, div: u32) -> bool {
        Payload::check_unit_error(error, div)
    }
}
impl Code for Payload {
    fn name() -> &'static str { "EAN" }
    fn unit_width() -> u32 { CODE_WIDTH }
    fn check_bar_error(error: u32, div: u32) -> bool {
        3 * error <= 2 * div
     }
    fn check_unit_error(error: u32, div: u32) -> bool {
        15 * error <= 4 * div
    }
}
const START_END_PATTERN: &'static [[u32; 3]] = &[[1, 1, 1]];
const MIDDLE_PATTERN: &'static [[u32; 5]] = &[[1, 1, 1, 1, 1]];

const CODE_PATTERN: &'static [[u32; CODE_BARS]] = &[
    [3, 2, 1, 1],
    [2, 2, 2, 1],
    [2, 1, 2, 2],
    [1, 4, 1, 1],
    [1, 1, 3, 2],
    [1, 2, 3, 1],
    [1, 1, 1, 4],
    [1, 3, 1, 2],
    [1, 2, 1, 3],
    [3, 1, 1, 2],
    [1, 1, 2, 3],
    [1, 2, 2, 2],
    [2, 2, 1, 2],
    [1, 1, 4, 1],
    [2, 3, 1, 1],
    [1, 3, 2, 1],
    [4, 1, 1, 1],
    [2, 1, 3, 1],
    [3, 1, 2, 1],
    [2, 1, 1, 3],
];

pub fn decode_13(line: &Line) -> Result<String, ()> {
    let start = line.next(0, true);
    let (mut start, _) = try!(line.find_patterns(start, Start, START_END_PATTERN));
    let mut result = String::with_capacity(13);
    let mut parity = 0;
    let mut checksum = 0;
    for i in 0..6 {
        let code = try!(line.match_patterns(start, Payload, CODE_PATTERN)) as u8;
        start += CODE_BARS;
        result.push((b'0' + code % 10) as char);
        if code < 10 {
            parity |= 1 << i;
        }
        checksum += code % 10 * if i % 2 == 1  { 1 } else { 3 };
    }
    let code = match parity {
        0b111111 => 0,
        0b001011 => 1,
        0b010011 => 2,
        0b100011 => 3,
        0b001101 => 4,
        0b011001 => 5,
        0b110001 => 6,
        0b010101 => 7,
        0b100101 => 8,
        0b101001 => 9,
        _ => return Err(())
    };
    result.insert(0, (b'0' + code) as char);
    checksum += code % 10;
    try!(line.match_patterns(start, Middle, MIDDLE_PATTERN));
    start += 5;
    for i in 0..6 {
        let code = try!(line.match_patterns(start, Payload, &CODE_PATTERN[..10])) as u8;
        start += CODE_BARS;
        result.push((b'0' + code) as char);
        checksum += code * if i % 2 == 1  { 1 } else { 3 };
    }
    if checksum % 10 == 0 {
        Ok(result)
    } else {
        Err(())
    }
}
pub fn decode_8(line: &Line) -> Result<String, ()> {
    let start = line.next(0, true);
    let (mut start, _) = try!(line.find_patterns(start, Start, START_END_PATTERN));
    let mut result = String::with_capacity(8);
    let mut checksum = 0;
    for i in 0..4 {
        let code = try!(line.match_patterns(start, Payload, &CODE_PATTERN[..10])) as u8;
        start += CODE_BARS;
        result.push((b'0' + code) as char);
        checksum += code % 10 * if i % 2 == 1  { 1 } else { 3 };
    }
    try!(line.match_patterns(start, Middle, MIDDLE_PATTERN));
    start += 5;
    for i in 0..4 {
        let code = try!(line.match_patterns(start, Payload, &CODE_PATTERN[..10])) as u8;
        start += CODE_BARS;
        result.push((b'0' + code) as char);
        checksum += code * if i % 2 == 1  { 1 } else { 3 };
    }
    if checksum % 10 == 0 {
        Ok(result)
    } else {
        Err(())
    }
}
