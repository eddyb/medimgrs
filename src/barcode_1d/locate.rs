use std::f64::INFINITY;

use angle::{Orientation, OrientationAverage};
use chunk::{self, component_moments, label_chunk, Label, Moments, Pixel, otsu_chunks, skeletonize_chunk};
use greyscale::Luma;
use union_find::UnionFind;

use image::{self, ImageBuffer};

#[derive(Copy, Clone, Default)]
pub struct ChunkInfo {
    label: usize,
    orientation: Orientation
}

#[derive(Copy, Clone)]
pub struct ChunkGroup {
    count: usize,
    orientation: OrientationAverage,
    sin: f64,
    cos: f64,
    min_x: f64,
    max_x: f64,
    min_y: f64,
    max_y: f64
}

impl ChunkGroup {
    /// Returns a (from, to) pair of coordinates corresponding
    /// to the line segment passing through the bounding box,
    /// orthogonal to the barcode.
    /// The offset is between [-1.0, 1.0] and controls the
    /// distance from the middle of the bounding-box to the
    /// line, where -1.0 is one of the box sides, 1.0 is the
    /// opposite side, and 0.0 passes through the middle.
    pub fn mid_line(&self, offset: f64) -> ([f64; 2], [f64; 2]) {
        // Extend the line in either direction by 10%.
        let ext = ((self.max_y - self.min_y) / 10.0).floor();

        let offset = (self.max_x - self.min_x) * offset / 2.0;
        let mid_x = (self.min_x + self.max_x) / 2.0 + offset;
        let mut points = [[mid_x, self.min_y - ext], [mid_x, self.max_y + ext]];

        // Rotate both points to image space.
        for point in &mut points {
            let [x, y] = *point;
            *point = [
                x * self.cos + y * -self.sin,
                x * self.sin + y * self.cos
            ];
        }

        (points[0], points[1])
    }
}

pub fn locate(image: &ImageBuffer<image::Luma<u8>, Vec<u8>>) -> Vec<ChunkGroup> {
    visualize! {
        "Grayscale image" => ImageLuma8(image.clone())
    };

    let width = image.width() as usize;
    let height = image.height() as usize;
    let start_row = (height % chunk::SIZE) / 2;
    let start_column = (width % chunk::SIZE) / 2;
    let chunks_per_row = width / chunk::SIZE;

    let mut chunks = otsu_chunks(image, width);
    visualize! {
        "Binary image" => ImageLuma8(chunk::chunks_to_image(&chunks, chunks_per_row))
    };

    let mut map = vec![0];
    let mut chunk_info = vec![ChunkInfo::default(); chunks.len()];
    let total = chunks.len();
    for (i, chunk) in chunks.iter_mut().enumerate() {
        // Ignore chunks on the border.
        if i < chunks_per_row || i > total - chunks_per_row {
            continue;
        }
        if i % chunks_per_row == 0 || (i + 1) % chunks_per_row == 0 {
            continue;
        }

        {
            let _v = visualize! {
                format!("Skeletonizing {}/{}", i, total) =>
                    Progress2D { width: chunks_per_row, current: i, total: total }
            };
            *chunk = skeletonize_chunk(*chunk);
        }

        let moments = {
            let _v = visualize! {
                format!("Labeling {}/{}", i, total) =>
                    Progress2D { width: chunks_per_row, current: i, total: total }
            };
            let labels = label_chunk(chunk);
            component_moments(&labels)
        };

        if let Some(orientation) = chunk_orientation(moments) {
            chunk_info[i].orientation = orientation;
            let neighbors = [
                i - chunks_per_row - 1,
                i - chunks_per_row,
                i - chunks_per_row + 1,
                i - 1
            ];
            for &j in &neighbors {
                if chunk_info[j].label != 0 {
                    if chunk_info[j].orientation.similar(chunk_info[i].orientation, 0.95) {
                        if chunk_info[i].label != 0 {
                            map.union(chunk_info[i].label, chunk_info[j].label);
                        } else {
                            chunk_info[i].label = chunk_info[j].label;
                        }
                    }
                }
            }
            if chunk_info[i].label == 0 {
                chunk_info[i].label = map.len();
                map.push(chunk_info[i].label);
            }
        }
    }

    visualize! {
        "Skeletonized image" => ImageLuma8(chunk::chunks_to_image(&chunks, chunks_per_row))
    };

    visualize! {
        "Labeled image" => Stacked(
            ImageLuma8(chunk::chunks_to_image(&chunks, chunks_per_row)),
            chunk_info.iter().enumerate().filter(|&(_, c)| c.label != 0).map(|(i, c)| {
                let x = start_column + i % chunks_per_row * chunk::SIZE;
                let y = start_row + i / chunks_per_row * chunk::SIZE;
                let bb = BB::rect(x, y, chunk::SIZE, chunk::SIZE).map(|x| x as f64);
                let color = Label(map.find(c.label) as u8 + 1).to_rgba();

                let r = chunk::SIZE as f64 / 2.0;

                // Coordinates for the center of the circle.
                let [x, y] = bb.top_left();
                let [x, y] = [x + r, y + r];

                // Grab the angle and compute the coordinates on the circle.
                let angle = c.orientation.to_angle();
                let [dx, dy] = [r * angle.cos(), r * angle.sin()];

                Stacked(BoundingBox(bb, 0.0, color),
                        Line([x - dx, y - dy], [x + dx, y + dy], color))
            }).collect::<Vec<_>>())
    };

    let mut groups = vec![ChunkGroup {
        count: 0,
        orientation: OrientationAverage::default(),
        sin: 0.0,
        cos: 0.0,
        min_x: INFINITY,
        max_x: -INFINITY,
        min_y: INFINITY,
        max_y: -INFINITY
    }; map.len()];
    for i in 0..chunks.len() {
        let info = &chunk_info[i];
        if info.label != 0 {
            let group = &mut groups[map.find(info.label)];
            group.count += 1;
            group.orientation.add(info.orientation);
        }
    }
    for group in &mut groups {
        let angle = group.orientation.compute().to_angle();
        group.sin = angle.sin();
        group.cos = angle.cos();
    }
    for i in 0..chunks.len() {
        if chunk_info[i].label == 0 {
            continue;
        }
        let group_label = map.find(chunk_info[i].label);
        let group = &mut groups[group_label];
        let ox = start_column + i % chunks_per_row * chunk::SIZE;
        let oy = start_row + i / chunks_per_row * chunk::SIZE;
        for &(x, y) in &[(0, 0), (1, 0), (0, 1), (1, 1)] {
            let x = (x * chunk::SIZE + ox) as f64;
            let y = (y * chunk::SIZE + oy) as f64;
            let rx = x * group.cos + y * group.sin;
            let ry = x * -group.sin + y * group.cos;
            if group.min_x > rx {
                group.min_x = rx;
            }
            if group.max_x < rx {
                group.max_x = rx;
            }
            if group.min_y > ry {
                group.min_y = ry;
            }
            if group.max_y < ry {
                group.max_y = ry;
            }
        }
    }
    groups.retain(|x| x.count >= 6 );
    groups.sort_by(|a, b| b.count.cmp(&a.count));
    for (i, group) in groups.iter().enumerate() {
        let orientation = group.orientation.compute();
        let color = Label(i as u8 + 1).to_rgba();
        visualize! {
            format!("Group with {} chunks", group.count) =>
                Stacked(ImageLuma8(image.clone()),
                Stacked(BoundingBox(BB {
                    x1: group.min_x, y1: group.min_y,
                    x2: group.max_x, y2: group.max_y
                }, orientation.to_angle(), color), {
                    let (from, to) = group.mid_line(0.0);
                    Line(from, to, color)
                }))
        };
    }
    groups
}

#[test]
fn test_locate() {
    let image = image::open("test/quagga/code_128/image-002.png").unwrap().to_luma();
    locate(&image);
}

#[bench]
fn bench_locate(b: &mut ::test::Bencher) {
    let image = image::open("test/quagga/code_128/image-002.png").unwrap().to_luma();
    b.iter(|| locate(&image));
}

struct Cluster {
    count: usize,
    orientation: OrientationAverage
}

fn chunk_orientation(moments: [Moments; 256]) -> Option<Orientation> {
    let mut clusters: Vec<Cluster> = vec![];
    let mut counter = 0;
    let mut total = 0;
    for i in 1..256 {
        if moments[i].count() == 0 {
            break;
        }
        total += 1;
        if moments[i].count() <= (chunk::SIZE + 2) / 3 {
            continue;
        }
        counter += 1;
        let mut found: bool = false;
        let orientation = moments[i].orientation();
        for cluster in &mut clusters {
            if cluster.orientation.compute().similar(orientation, 0.9) {
                cluster.count += 1;
                cluster.orientation.add(orientation);
                found = true;
                break;
            }
        }
        if !found {
            let mut average = OrientationAverage::default();
            average.add(orientation);
            clusters.push(Cluster {
                count: 1,
                orientation: average
            });
        }
    }

    if counter < 2 {
        return None;
    }

    if let Some(cluster) = clusters.iter().max_by(|c| c.count) {
        if cluster.count > total / 4 && cluster.count >= counter * 3 / 4 {
            return Some(cluster.orientation.compute());
        }
    }

    None
}
