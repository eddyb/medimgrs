use std::ops::Range;
use std::u32;

use angle::{Ratio, bresenham_points, BresenhamPoints};
use greyscale::Luma;

use image::{self, ImageBuffer};

/// Common operations for decoding the various 1D barcodes.
pub trait Code {
    // TODO(eddyb) associated constants here.
    /// A human-readable name of this code type (for visualization).
    fn name() -> &'static str;

    /// The total width (sum of counts) for a single
    /// "code unit" in the barcode.
    fn unit_width() -> u32;

    /// Returns true if the error of a single bar is acceptable.
    fn check_bar_error(error: u32, div: u32) -> bool {
        error <= 1 * div
    }

    /// Returns true if the average error of the bars in an unit is acceptable.
    fn check_unit_error(error: u32, div: u32) -> bool {
        2 * error <= div
    }
    /// Try to match a (normalized, div) pair of normalized counts with
    /// a normal-form integer pattern. The normalized and pattern slices
    /// are expected to be the same length, with sum(slice[i]) = unit_width
    /// and min(slice[i]) >= 1.
    /// The returned error is in units of unit_width * div, which means only
    /// error values from the same normalized set of counts are comparable.
    fn match_pattern(normalized: &[u32], div: u32, pattern: &[u32]) -> Ratio {
        let mut error = 0;
        for (&a, &b) in normalized.iter().zip(pattern.iter()) {
            let b = b * div;
            let delta = if a > b { a - b } else { b - a };
            if !Self::check_bar_error(delta, div) {
                return Ratio { num: u32::MAX, div: div * Self::unit_width() };
            }
            error += delta;
        }
        if !Self::check_unit_error(error, div * Self::unit_width()) {
            error = u32::MAX;
        }
        Ratio {
            num: error,
            div: div * Self::unit_width()
        }
    }
}


const SAMPLES: usize = 4;

#[derive(Clone, Default)]
pub struct Line {
    start_value: bool,
    slope: f64,
    counts: Vec<u32>
}

impl Line {
    pub fn sample(image: &ImageBuffer<image::Luma<u8>, Vec<u8>>,
                  from: [f64; 2], to: [f64; 2])
                  -> Line {
        let (w, h) = image.dimensions();
        let at = |x, y| {
            if x >= 0 && y >= 0 {
                let (x, y) = (x as u32, y as u32);
                if x < w && y < h {
                    image[(x, y)].data[0]
                } else {
                    255
                }
            } else {
                255
            }
        };

        let (mut min, mut max) = (255, 0);

        // Sample values in the image along the line.
        let points = bresenham_points(from, to, SAMPLES);
        let BresenhamPoints { steep, reversed, slope, y_step, .. } = points;
        let data: Vec<u8> = points.map(|(x, y, y_frac)| {
            let half = y_frac.div / 2;
            let (a, b, lerp) = if y_frac.num < half {
                (-y_step, 0, y_frac.num + half)
            } else {
                (0, y_step, y_frac.num - half)
            };

            // Get the two pixel values.
            let (a, b) = {
                let get = |dy| {
                    if steep {
                        at(y + dy, x) as u32
                    } else {
                        at(x, y + dy) as u32
                    }
                };
                (get(a), get(b))
            };

            // Interpolate the two pixel values.
            let v = a * (y_frac.div - lerp) + b * lerp;
            let v = (v / y_frac.div) as u8;

            if v < min {
                min = v;
            }
            if v > max {
                max = v;
            }

            v
        }).collect();

        let center = min + (max - min) / 2;
        let center_1_5 = (center as u32) * 3 / 2;
        let center_1_5 = if center_1_5 >= 255 { 255 } else { center_1_5 as u8 };
        let threshold = (max - min) / 12;

        // Find extrema in the line.
        let mut currently_up = data[0] > center;
        let mut extrema = Vec::with_capacity(4);
        extrema.push((0, data[0]));
        for i in 0..data.len()-2 {
            let (a, b, c) = (data[i], data[i + 1], data[i + 2]);
            let up = if a >= c && a - c > threshold && b < center_1_5 {
                false
            } else if c >= a && c - a > threshold && b > center / 2 {
                true
            } else {
                continue;
            };
            if up != currently_up {
                extrema.push((i, a));
                currently_up = up;
            }
        }
        extrema.push((data.len(), data[data.len() - 1]));

        // Binarize the line using averages between extrama as thresholds,
        // and count lengths of alternating runs of black and white bars.
        let start_value = data[0] <= center;
        let mut current = start_value;

        let mut counts = Vec::with_capacity(16);
        counts.push(1);

        for j in 1..extrema[1].0 {
            let v = data[j] <= center;
            if v == current {
                *counts.last_mut().unwrap() += 1;
            } else {
                counts.push(1);
                current = v;
            }
        }

        for i in 1..extrema.len()-1 {
            let (a, b) = (extrema[i].1, extrema[i + 1].1);

            let threshold = if b > a {
                a + (((b as u32 - a as u32) * 2 / 3) as u8)
            } else {
                b + (a - b) / 3
            };

            for j in extrema[i].0..extrema[i + 1].0 {
                let v = data[j] <= threshold;
                if v == current {
                    *counts.last_mut().unwrap() += 1;
                } else {
                    counts.push(1);
                    current = v;
                }
            }
        }


        let mut line = Line {
            start_value: start_value,
            slope: slope,
            counts: counts
        };

        if reversed {
            line.reverse();
        }

        line
    }

    pub fn len(&self) -> usize {
        self.counts.len()
    }

    pub fn value(&self, i: usize) -> bool {
        self.start_value ^ ((i & 1) == 1)
    }

    pub fn next(&self, start: usize, v: bool) -> usize {
        start + (self.value(start) ^ v) as usize
    }

    pub fn count(&self, i: usize) -> u32 {
        if i >= self.counts.len() {
            0
        } else {
            self.counts[i]
        }
    }

    pub fn range_count(&self, r: Range<usize>) -> u32 {
        r.map(|i| self.count(i)).sum()
    }

    pub fn width(&self, i: usize) -> f64 {
        self.count(i) as f64 / SAMPLES as f64 * self.slope
    }

    pub fn range_width(&self, r: Range<usize>) -> f64 {
        self.range_count(r) as f64 / SAMPLES as f64 * self.slope
    }

    /// Normalize the counts such that the total length is the unit_width,
    /// and the shortest individual bar length is 1. The lengths are
    /// not divided, instead they are kept in integer form, and the
    /// divisor is returned. Err is returned if normalized.len()
    /// counts are not available after start.
    fn normalize<C: Code>(&self, start: usize, normalized: &mut [u32])
                          -> Result<u32, ()> {
        let counts = &self.counts[start..];
        if counts.len() < normalized.len() {
            return Err(());
        }

        let (mut ones, mut total) = (0, 0);
        for &c in &counts[..normalized.len()] {
            if c == 1 {
                ones += 1;
            }
            total += c;
        }

        if total > C::unit_width() {
            let mul = C::unit_width() - ones;
            let div = total - ones;
            for (norm, &c) in normalized.iter_mut().zip(counts.iter()) {
                *norm = if c == 1 { div } else { c * mul };
            }
            Ok(div)
        } else {
            let mul = C::unit_width();
            let div = total;
            for (norm, &c) in normalized.iter_mut().zip(counts.iter()) {
                *norm = c * mul;
            }
            Ok(div)
        }
    }

    /// Pick the best fit pattern and return its index and the error.
    pub fn match_patterns<A, C: Code>(&self, start: usize, _: C, patterns: &[A])
                                      -> Result<usize, ()>
    where A: Default + AsRef<[u32]> + AsMut<[u32]> {
        let mut normalized = A::default();
        let div = try!(self.normalize::<C>(start, normalized.as_mut()));
        let mut best_error = Ratio { num: u32::MAX, div: 0 };
        let mut best_code = Err(());
        for (code, pattern) in patterns.iter().enumerate() {
            let error = C::match_pattern(normalized.as_ref(), div, pattern.as_ref());
            if error.num < best_error.num {
                best_error = error;
                best_code = Ok(code);
            }
        }

        /*if let Ok(code) = best_code {
            visualize!{
                format!("Matched {}/{} => {:?}", C::name(), code, patterns[code].as_ref()) =>
                    At([self.range_width(0..start) as f64, 0.0], self::Line {
                        start_value: self.value(start),
                        slope: self.slope,
                        counts: self.counts[start..][..normalized.as_ref().len()].to_vec()
                    })
            };
        }*/

        best_code
    }

    pub fn find_patterns<A, C: Code + Default>(&self, mut start: usize, _: C, patterns: &[A])
                                               -> Result<(usize, usize), ()>
    where A: Default + AsRef<[u32]> + AsMut<[u32]>  {
        let code_bars = A::default().as_ref().len();
        while start + code_bars <= self.len() {
            if let Ok(code) = self.match_patterns(start, C::default(), patterns) {
                return Ok((start + code_bars, code));
            }
            // Skip to the next black bar.
            start += 2;
        }
        Err(())
    }

    pub fn decode_any(&self) -> Result<String, ()> {
        let decoders: &[fn(_) -> _] = &[
            ::barcode_1d::code_128::decode,
            ::barcode_1d::ean::decode_13,
            ::barcode_1d::ean::decode_8
        ];

        for decoder in decoders {
            if let Ok(r) = decoder(&self) {
                return Ok(r);
            }
        }

        Err(())
    }

    pub fn reverse(&mut self) {
        self.counts.reverse();
        self.start_value ^= self.len() & 1 == 0;
    }
}
