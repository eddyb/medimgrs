use image::{self, ImageBuffer};

pub mod locate;
pub mod decode;

// All the various 1D barcode format decoders.
pub mod code_128;
pub mod ean;

/// The number of lines to look at, inside the bounding box.
const SLICES: usize = 16;

pub fn find(image: &ImageBuffer<image::Luma<u8>, Vec<u8>>) -> Vec<String> {
    let groups = locate::locate(image);
    let mut results = vec![];
    for (i, group) in groups.iter().enumerate() {
        for slice in 0..(SLICES+1) {
            let offset = ((slice + 1) / 2) as f64 * 2.0 / SLICES as f64;
            let offset = if slice % 2 == 0 { -offset } else { offset };
            let (from, to) = group.mid_line(offset);
            let mut line = decode::Line::sample(image, from, to);

            let _v = visualize! {
                format!("Group #{}, offset {}", i, offset)
                    => line.clone()
            };

            if let Ok(r) = line.decode_any() {
                drop(_v);
                visualize! {
                    format!("Group #{}, offset {}: {}", i, offset, r)
                        => line.clone()
                };
                results.push(r);
                break;
            }

            // Try in reverse.
            line.reverse();

            drop(_v);
            let _v = visualize! {
                format!("Group #{}, offset {} (reverse)", i, offset)
                    => line.clone()
            };

            if let Ok(r) = line.decode_any() {
                drop(_v);
                visualize! {
                    format!("Group #{}, offset {} (reverse): {}", i, offset, r)
                        => line.clone()
                };
                results.push(r);
                break;
            }
        }
    }

    results
}

#[bench]
fn bench_find(b: &mut ::test::Bencher) {
    let image = image::open("test/quagga/code_128/image-002.png").unwrap().to_luma();
    b.iter(|| find(&image));
}
